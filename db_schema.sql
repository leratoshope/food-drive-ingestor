SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `leratos-hope`;
CREATE DATABASE `leratos-hope` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `leratos-hope`;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `products` (`id`, `name`) VALUES
(121, 'Box of soup'),
(4, 'Brown Bread'),
(150, 'Coffee 1kg'),
(122, 'Coffee Creamer'),
(123, 'Dried beans'),
(124, 'Jam 400g'),
(125, 'Jam 900g'),
(126, 'Maize 1kg'),
(127, 'Maize 2'),
(128, 'Oats 1kg'),
(2, 'Oats 2kg'),
(129, 'Oats 500g'),
(130, 'Oil 2L'),
(1, 'Oil 750ml'),
(131, 'Pasta'),
(132, 'Peanut butter 400g'),
(133, 'Peanut butter 800g'),
(134, 'Pilchards 215g'),
(135, 'Pilchards 425g'),
(136, 'Pkt of soup'),
(137, 'Rice 1kg'),
(138, 'Rice 2'),
(139, 'Rice 500g'),
(3, 'Rice 5kg'),
(140, 'Samp 2.5kg'),
(141, 'Samp and Beans'),
(142, 'Soup mix'),
(143, 'Spaghetti Meatballs Tins 410kg'),
(144, 'Sugar 1kg'),
(145, 'Sugar 2'),
(146, 'Sugar 500g'),
(147, 'Teabags - 250'),
(148, 'Tin of beans'),
(149, 'Tinned Pineapple');