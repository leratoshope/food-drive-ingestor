# Food Drive Ingestor #

![](https://img.shields.io/badge/author-jonogould-blue.svg)

The Food Drive Ingestor uses a custom form built on top of [Formstack](http://www.formstack.com)'s backend
form platform. All data is sent to Formstack via their [PHP API wrapper](https://github.com/formstack/formstack-api).

## Requirements

This site was created using the following, which are requirements in order for this project to run:

1. PHP 5.6
2. MySql
3. PHP Composer
4. Nginx

## Config

All configuration variables required to run the app can be found in the `/app/config`:

1. You will need to create `/app/config/env.php`, which you can do using `/app/config/env.sample.php`
2. Ensure that you change the relevant fields (db, etc)
3. Configure your routes using `/app/config/routes.php`
4. Run `composer update` from the root of the project folder
5. Add the location to your nginx log:

```
  server {
    listen       80;
    server_name  ingestor;

    root   /var/www/food-drive/public;
    index  index.php index.html index.htm;

    location / {
      try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
      include fastcgi.conf;
      fastcgi_intercept_errors on;
      fastcgi_pass 127.0.0.1:9000;
      fastcgi_index index.php;
      fastcgi_read_timeout 300;
    }
  }
```

## Database

The SQL statements required to create and seed the `leratos-hope` database can be
found in `/db_schema.sql`. After the database is created, a single table called 
`products` will be created and filled.

## Controllers

Controllers are created in the `/app/controllers/` folder. Map your routes to your 
controllers using dot notation.

E.g. route `/dashboard/test` will take you to the `test` function in the  `dashboard` controller.

## Models

Models are created in the `/app/models/` folder.

## Views

Views are created in the `/app/views/` folder. They use `twig` as the rendering engine.