<?php
namespace LH\Ingestor;

class Router extends Container {

  private $routes = [];
  private $args = [];

  /**
   * Set the default route for the router
   *
   * @param   string  $route      The default route
   *
   * @return  obj     $this       This router object
   */
  public function setDefaultRoute($route) {
    $this->routes['/'] = ['endpoint' => $route, 'verb' => 'GET'];

    return $this;
  }

  /**
   * Set the default route for the router
   *
   * @param   string  $url        The route url
   * @param   string  $path       Controller mapping (date-notated)
   * @param   string  $verb       The type of HTTP request
   *
   * @return  obj     $this       This router object
   */
  public function add($url, $path, $verb = 'GET') {
    $this->routes[$url] = ['endpoint' => $path, 'verb' => $verb];

    return $this;
  }

  /**
   * Resolves a route url
   *
   * @param   string  $path       The dot-notated route
   *
   * @return  void
   */
  public function resolve($path = null) {
    return $this->get($this->returnArrayFromDotNotation($this->findMatch($path)));
  }

  /**
   * Magically finds a match between the url and the defined routes (or 404s)
   *
   * @param   string  $path       The dot-notated route
   *
   * @return  void
   */
  protected function findMatch($path) {
    $parts = substr($path, 1);
    
    do {
      if (in_array('/' . $parts, array_keys($this->routes))) {
        $this->args = $this->splitArgs(str_replace((empty($parts) ? '' : '/') . $parts, '', $path));
        return $this->routes['/' . $parts]['endpoint'];
      }

      $parts = substr($parts, 0, strrpos($parts, '/'));
    } while (strrpos('/' . $parts, '/') !== false);

    return false;
  }

  /**
   * Turns any args from the URI into an array of args
   *
   * @param   string  $args       String representation of arguments
   *
   * @return  array   $result     The split out array of arguments
   */
  protected function splitArgs($args) {
    $result = [];

    $parts = explode('/', $args);

    foreach ($parts as $arg) {
      if (strpos($arg, ':') !== false) {
        $split = explode(':', $arg);
        $result[$split[0]] = $split[1];
      }
    }

    return $result;
  }

  /**
   * Calls the specific method in the specifically called controller (if exists)
   *
   * @param   array   $path       The split-up route
   *
   * @return  void
   */
  public function get($path) {
    if (class_exists($path[0])) {
      $res = (new $path[0])->setApp($this->app);
    } else {
      $res = (new Controller)->setApp($this->app);
    }

    //  Now call the requested function, fall back to index
    if (method_exists($res, $path[1])) {
      call_user_func([$res, $path[1]], $this->args);
    } else if (method_exists($res, 'index')) {
      call_user_func([$res, 'index'], $this->args);
    } else {
      call_user_func([$res, 'not_found'], $this->args);
    }
  }

  /**
   * Returns an array from a dot-notated string
   *
   * @param   string   $dotNotation       The dot-notated string
   *
   * @return  array                       The array representing the dot-notated string
   */
  protected function returnArrayFromDotNotation($dotNotation) {
    $array = explode('.', ucfirst($dotNotation));

    if (! isset($array[1]))
      $array[1] = '';

    return array_splice($array, 0, 2);
  }

  /**
   * Redirects the application to the specified url
   *
   * @param   string  $url        The url to send the application to
   * @param   array   $data       The data to attach to the end of the url
   * @param   bool    $internal   Whether to send the user to an external url
   *
   * @return  void
   */
  public function redirect($url, $data = [], $internal = true) {
    if (! $internal) {
      header('Location: ' . $url);
    } else {
      $url = $this->returnArrayFromDotNotation($url);

      if (empty($url[1])) {
        $url = $url[0];
      } else {
        $url = implode('/', $url);
      }

      //  Now add the data to the mix
      $args = '';

      foreach ($data as $key => $value) {
        $arg = $key . ':' . $value;
        $args[] = $arg;
      }

      if (!empty($args)) {
        $args = '/' . implode('/', $args);
      }
      
      header('Location: ' . $this->app->config['url'] . strtolower($url) . $args);
    }

    exit;
  }
}