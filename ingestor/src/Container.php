<?php
namespace LH\Ingestor;

class Container {
  use ApplicationAware;

  protected $config;

  public function __get($name) {
    if (isset($this->$name)) {
      return $this->$name;
    }
  }

  public function __set($name, $value) {
    $this->$name = $value;

    return $this;
  }

  public function __isset($name) {
    return isset($this->$name);
  }

  public function __construct() {
    return $this;
  }
}