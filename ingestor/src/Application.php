<?php
namespace LH\Ingestor;

class Application extends Container {

  protected $config;
  protected $db;
  protected $router;

  public function __construct(array $config = []) {
    if (!is_null($config)) {
      $this->config = $config;
    }

    $this->registerServices();

    return $this;
  }

  public function __invoke() {
    $this->run();
  }

  /**
   * Start resolving routes
   *
   * @return  void
   */
  public function run() {
    $this->resolveRoute();
  }

  /**
   * Registeres all the services required for the app
   *
   * @return  obj     $this       The application object
   */
  public function registerServices() {
    $this->registerCustomAutoloader();

    $this->router = (new Router())
      ->setApp($this)
      ->setDefaultRoute($this->config['default']);
    
    $this->db = new \PDO(
      "mysql:charset=utf8mb4;host={$this->config['db']['DB_HOST']};
      dbname={$this->config['db']['DB_NAME']}", $this->config['db']['DB_USER'],
      $this->config['db']['DB_PASS'], [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_PERSISTENT => true
      ]
    );

    $this->db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

    return $this;
  }

  /**
   * Registers a custom autoloader for all project specific models
   *
   * @return  bool            Whether or not the autoloaded class was found
   */
  public function registerCustomAutoloader() {
    spl_autoload_register(function($class) {
      //  List of folders to search through
      $class_dirs = ['/app/controllers/', '/app/models/', '/app/middleware/'];

      //  Remove namespace from class
      $backslash = strrpos($class, '\\');
      if ($backslash > -1) $class = substr($class, $backslash+1);

      foreach ($class_dirs as $dir) {
        $path = $this->config['base'] . $dir . strtolower($class) . '.php';

        if (file_exists($path)) {
          @include_once ($path);

          return true;
        }
      }
    });
  }

  /**
   * Resolves a route using the Router object
   *
   * @return  void
   */
  public function resolveRoute() {
    $this->router->resolve($_SERVER['REQUEST_URI']);
  }
}