<?php
namespace LH\Ingestor;

trait ApplicationAware {
  protected $app;

  /**
   * Make a Container aware of the Application
   *
   * @param   obj     $app        The application object
   *
   * @return  obj     $this       The just loaded object that this trait is attached to
   */
  public function setApp(Application $app) {
    $this->app = $app;

    return $this;
  }
}