<?php
namespace LH\Ingestor;

class View {
  protected $config = [];
  protected $data;
  protected $twig;

  public function __construct() {
    $dir = __DIR__ . '/../../app/views';

    //  Twig stuff
    $loader = new \Twig_Loader_Filesystem($dir);
    $loader->addPath($dir . '/pages', 'pages');
    $loader->addPath($dir . '/partials', 'partials');
    $loader->addPath($dir . '/forms', 'forms');

    if (true) {
      $this->twig = new \Twig_Environment($loader, [ 'debug' => true ]);
      $this->twig->addExtension(new \Twig_Extension_Debug());
    }
    else {
      $this->twig = new \Twig_Environment($loader, [ 'cache' => '/tmp/twig_cache' ]);
    }
  }

  /**
   * Load a view to the browser
   *
   * @param   string  $page       The page to load
   * @param   array   $data       The data to attatch to the page
   *
   * @return  void
   */
  public function load($page, $data = []) {
    echo $this->twig->render($page, $data);
  }
}