<?php
namespace LH\Ingestor;

class Controller extends Container {
  
  protected $view;

  public function __construct() {
    $this->view = new View;
    
    return $this;
  }

  /**
   * Sends a load request to the View container
   *
   * @param   string  $page       The page to be displayed
   * @param   array   $data       The data to attach to the page
   *
   * @return  void
   */
  public function render($page, $data = []) {
    $this->view->load($page . '.html', $data);
  }

  /**
   * A quick 404 function for if a page is not found
   *
   * @param   string  $path       The path that wasn't found
   *
   * @return  void
   */
  public function not_found($path) {
    header("HTTP/1.0 404 Not Found");
    echo sprintf('404: %s not found.', implode('/', $path));

    return;
  }

  /**
   * An alias for the Router's redirect function
   *
   * @param   string  $url        The url to send the application to
   * @param   array   $data       The data to attach to the end of the url
   * @param   bool    $internal   Whether to send the user to an external url
   *
   * @return  void
   */
  public function redirect($url, $data = [], $internal = true) {
    return $this->app->router->redirect($url, $data, $internal);
  }
}