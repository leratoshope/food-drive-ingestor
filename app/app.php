<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';

//  Create a new Ingestor application
$app = new LH\Ingestor\Application(
  require_once __DIR__ . '/../app/config/env.php'
);

//  Make the routes known to the application
require_once __DIR__ . '/../app/config/routes.php';

//  Lets get going
$app();