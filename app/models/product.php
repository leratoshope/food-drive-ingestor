<?php

class Product {
  private $id;
  private $name;

  private $db;

  public function __get($name) {
    if (isset($this->$name)) {
      return $this->$name;
    }
  }

  public function __set($name, $value) {
    $this->$name = $value;

    return $this;
  }

  public function __isset($name) {
    return isset($this->$name);
  }

  public function __construct(\PDO $db, $id = null) {
    $this->db = $db;

    if (is_numeric($id)) {
      $this->load($id);
    }
  }

  /**
   * Load a product from the database
   *
   * @param   int     $id         The ID of product
   *
   * @return  obj     $this       The just loaded product object
   */
  public function load($id) {
    $this->id = $id;

    $query = $this->db->prepare('SELECT name FROM products WHERE id = :id');
    $query->setFetchMode(\PDO::FETCH_INTO, $this);
    $query->execute(['id' => $id]);

    return $this;
  }

  /**
   * Load a product from the database by the product name
   *
   * @param   string  $name       The name of product
   *
   * @return  obj     $this       The just loaded product object
   */
  public function load_by_name($name) {
    $query = $this->db->prepare('SELECT id FROM products WHERE name = :name');
    $query->execute(['name' => $name]);

    if ($query->rowCount() === 0) {
      return null;
    }

    $this->id = $query->fetch(\PDO::FETCH_COLUMN);
    $this->name = $name;

    return $this;
  }

  /**
   * Create a new product and insert it into the database
   *
   * @return  int     $id       The ID of the newly created product
   */
  public function create() {
    $insert = $this->db->prepare('INSERT INTO products (name) VALUES (:name)');
    $insert->execute(['name' => $this->name]);

    $this->id = $this->db->lastInsertId();

    return $this->id;
  }

  /**
   * Loads all products from the database
   *
   * @param   obj     $pdo        PDO object
   * @param   bool    $refresh    Get an uncached version of this script
   *
   * @return  obj                 An array of product names
   */
  public static function findAll(\PDO $db, $refresh = false) {
    $products = [];

    if ($refresh || empty($products)) {
      $query = $db->prepare('SELECT id, name FROM products ORDER BY name ASC');
      $query->execute();

      while ($row = $query->fetch(\PDO::FETCH_OBJ)) {
        $product = new self($db);
        $product->id = $row->id;
        $product->name = $row->name;

        $products[] = $product->name;
      }
    }

    return $products;
  }
}