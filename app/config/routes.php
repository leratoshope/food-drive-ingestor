<?php

/*=============================
 *  Define your routes
 ============================*/

$app->router
  ->add('/', 'dashboard', 'GET')
  ->add('/submit', 'dashboard.submit', 'POST')
  ->add('/thanks', 'dashboard.thanks', 'GET')
  ->add('/submissions', 'dashboard.submissions', 'GET');