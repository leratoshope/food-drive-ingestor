<?php

/*===============================================
 *  Replace these config vars as needed
 ==============================================*/

date_default_timezone_set('UTC');

$base = realpath(__DIR__ . '/../../');

return [
  'base' => $base,
  'url' => 'http://localhost:8555/',
  'default' => 'dashboard.index',
  'paths' => [
    'models'      => $base . '/app/models/',
    'views'       => $base . '/app/views/',
    'controllers' => $base . '/app/controllers/'
  ],
  'db' => [
    'DB_HOST' => 'localhost',
    'DB_NAME' => 'leratos-hope',
    'DB_USER' => 'root',
    'DB_PASS' => ''
  ],
  'formstack' => [
    'accessToken' => '6418471b620473aeaaa6e92cff30a7a1'
  ]
];