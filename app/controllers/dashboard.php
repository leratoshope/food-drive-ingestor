<?php

class Dashboard extends LH\Ingestor\Controller {
  private $formID = 2036166;
  
  private $formFields = [
    'date'      => '33292565',
    'product'   => '33292582',
    'quantity'  => '33292577'
  ];

  /**
   * The form page
   */
  public function index($args = []) {
    $fails = [];
    $message = '';

    if (isset($args['result']) && $args['result'] === 'fail') {
      $message = ['text' => 'Your form could not be submitted at this time. Please try again later.', 'mood' => 'danger'];
      $fails = explode(',', $args['fails']);
    }

    $products = \Product::findAll($this->app->db);

    $data = [
      'meta' => [
        'url' => $this->app->config['url'],
        'header' => ['food' => true, 'submissions' => false],
        'message' => $message
      ],
      'data' => [
        'fields' => [
          'date' => ['value' => date('Y-m-d'), 'fail' => in_array('date', $fails)],
          'products' => ['values' => $products, 'fail' => in_array('product', $fails)],
          'custom_product' => ['checked' => false],
          'new_product' => ['value' => '', 'fail' => in_array('new_product', $fails)],
          'quantity' => ['value' => 0, 'fail' => in_array('quantity', $fails)]
        ],
      ]
    ];


    $this->render('pages/dashboard', $data);
  }

  /**
   * When a form gets submitted
   */
  public function submit() {
    $formstack = new FormstackApi($this->app->config['formstack']['accessToken']);
    $successfulSubmission = true;

    //  Ensure that all the data is there
    $fails = array_diff(array_keys($this->formFields), array_keys($_POST));

    //  If a custom product is entered, add it to the db
    if (isset($_POST['custom_product']) && $_POST['custom_product'] === 'on' 
      && !empty($_POST['new_product'])) {
      try {
        $product = new \Product($this->app->db);
        $product->name = $_POST['new_product'];
        $product->create();
      } catch(\Exception $e) {}

      $product = $_POST['new_product'];
    } else {
      $product = $_POST['product'];
    }

    //  Check for a product
    if (empty($product)) {
      $fails[] = 'product';
    }

    //  If something hasn't passed the above tests, return them
    if (!empty($fails)) {
      $this->redirect('result:fail', ['fails' => implode(',', $fails)]);
    }

    $formData = [
      '33292565' => date('d M Y', strtotime($_POST['date'])),
      '33292582' => $product,
      '33292577' => $_POST['quantity']
    ];

    try {
      $result = $formstack->submitFormAssoc($this->formID, $formData);
    } catch (\Exception $e) {
      $successfulSubmission = false;
      $this->redirect('result:fail');
    }

    $this->redirect('thanks');
  }

  /**
   * The form thank you page
   */
  public function thanks() {
    $this->render('pages/thanks', ['meta' => ['url' => $this->app->config['url'],
      'header' => ['food' => true, 'submissions' => false]]]);
  }

  /**
   * A page of all form submissions
   */
  public function submissions() {
    $formstack = new FormstackApi($this->app->config['formstack']['accessToken']);
    $data = [
      'meta' => [
        'url' => $this->app->config['url'],
        'header' => ['food' => false, 'submissions' => true]
      ],
      'items' => []
    ];

    try {
      $submissions = $formstack->getSubmissions($this->formID, '', '', '', [], [], 1, 25, 'DESC', true);

      foreach ($submissions as $submission) {
        $raw = (array)$submission->data;
        $item = new \stdClass;

        foreach ($raw as $obj) {
          $name = array_search($obj->field, $this->formFields);
          $item->$name = $obj->value;
        }

        $data['items'][] = $item;
      }
    } catch (\Exception $e) {}

    $this->render('pages/submissions', $data);
  }
}