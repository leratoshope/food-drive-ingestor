/**
 *  main.js
 *
 *  Code used to make the map page work
 */

(function($) {
  $('.btn-add-product').on('click', function() {
    $('#custom_product').prop('checked', true);
    $('.new-product').removeClass('hide');
    $('.existing-product').addClass('hide');
  });
})(jQuery);